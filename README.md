# lpc55prog

`lpc55prog` was built as a tool for flashing NXP LPC5526/28 microcontrollers
using their built-in bootloader ROM's USB ISP mode. Work was started on this
tool as I couldn't find an official or community tool for doing so.

Later I discovered the very poorly named/discoverable `bltool` that implements
this protocol (which I have [mirrored](https://git.gotroot.ca/ktims/blhost) in
my forge).

## Features

`lpc55prog` can read, write and erase the flash of LPC5526, LPC5528, LPC55S26 &
LPC55S28 microcontrollers using the USB ISP. Object files can be read or written
in raw binary or Intel hex, and can also be written to simple ELF.

It has only been tested on Linux, but will likely also work on other *nix
systems like macOS. Windows might work, please let me know if it does 🙂.

## Installation

```shell
git clone https://git.gotroot.ca/ktims/lpc55prog
cargo install --path lpc55prog
```

## Usage

See `lpc55prog help`.