use std::fs::File;
use std::io::prelude::*;
use std::iter::repeat;
use std::path::PathBuf;

use clap::ValueEnum;
use log::{debug, warn};
use object::elf;
use object::Endianness;

use ihex;
use object::write::elf::{FileHeader, SectionHeader};

// Perhaps should refactor so we don't have UI ValueEnum here?
#[derive(Debug, Clone, ValueEnum)]
pub enum MemoryFileType {
    Elf,
    Ihex,
    Bin,
}

pub struct MemorySection {
    pub address: u32,
    pub data: Vec<u8>,
}

const M33_FLAGS: u32 = elf::EF_ARM_EABI_VER5 | elf::EF_ARM_ABI_FLOAT_HARD; // processor has hard float, even if code may not use it

struct ElfWriter;
struct IhexWriter;
struct BinWriter;

struct ElfReader {
    file_contents: Vec<u8>,
}
struct IhexReader {
    file_contents: String,
}
struct BinReader {
    filesize: usize,
    file: File,
}

/// Build a new object to manage writing type T
impl MemoryFileType {
    fn type_from_filename(filename: &str) -> Option<MemoryFileType> {
        let filename = filename.to_lowercase();
        let (_, extension) = filename.rsplit_once('.').unwrap_or(("", ""));
        match extension {
            "elf" => Some(MemoryFileType::Elf),
            "hex" | "ihex" => Some(MemoryFileType::Ihex),
            "bin" | "raw" => Some(MemoryFileType::Bin),
            _ => None,
        }
    }
    pub fn object_file_reader_auto(
        filename: &PathBuf,
    ) -> Result<Box<dyn ObjectFileReader>, std::io::Error> {
        match MemoryFileType::type_from_filename(filename.to_str().unwrap_or("")) {
            Some(MemoryFileType::Elf) => {
                unimplemented!("Sorry ELF reading support is coming later")
            }
            Some(MemoryFileType::Ihex) => IhexReader::new(filename),
            Some(MemoryFileType::Bin) => BinReader::new(filename),
            None => {
                warn!("Can't determine an appropriate output type based on filename {}, reading as raw binary", filename.to_str().unwrap_or("<invalid utf-8>"));
                unimplemented!()
            }
        }
    }
    pub fn object_file_reader(
        &self,
        filename: &PathBuf,
    ) -> Result<Box<dyn ObjectFileReader>, std::io::Error> {
        Ok(match *self {
            MemoryFileType::Elf => unimplemented!("Sorry ELF reading support is coming later"),
            MemoryFileType::Ihex => IhexReader::new(filename)?,
            MemoryFileType::Bin => BinReader::new(filename)?,
        })
    }
    pub fn mem_writer(&self) -> Box<dyn MemoryWriter> {
        match *self {
            MemoryFileType::Elf => Box::new(ElfWriter),
            MemoryFileType::Ihex => Box::new(IhexWriter),
            MemoryFileType::Bin => Box::new(BinWriter),
        }
    }
    pub fn mem_writer_for_file(filename: &PathBuf) -> Box<dyn MemoryWriter> {
        match MemoryFileType::type_from_filename(filename.to_str().unwrap_or("")) {
            Some(MemoryFileType::Elf) => Box::new(ElfWriter),
            Some(MemoryFileType::Ihex) => Box::new(IhexWriter),
            Some(MemoryFileType::Bin) => Box::new(BinWriter),
            None => {
                warn!("Can't determine an appropriate output type based on filename {}, writing as raw binary", filename.to_str().unwrap_or("<invalid utf-8>"));
                Box::new(BinWriter)
            }
        }
    }
}

/// An object that can read a block of memory from an object file
pub trait ObjectFileReader {
    fn new(file: &PathBuf) -> Result<Box<dyn ObjectFileReader>, std::io::Error>
    where
        Self: Sized;
    /// Return the base address and entire data of `text` sections in the object file,
    /// copied into a Vec<u8>. This will allocate the necessary space for the entire memory
    /// map of the `text` sections, which in some cases is probably a horrible idea.
    fn read_all(&mut self, size: Option<u32>) -> Result<MemorySection, Box<dyn std::error::Error>>;
}

/// An object that can write a block of memory to a file
pub trait MemoryWriter {
    fn write_mem(
        &mut self,
        output: &mut dyn std::io::Write,
        addr: u32,
        data: &[u8],
    ) -> Result<(), Box<dyn std::error::Error>>;
}

impl MemoryWriter for ElfWriter {
    fn write_mem(
        &mut self,
        output: &mut dyn std::io::Write,
        addr: u32,
        data: &[u8],
    ) -> Result<(), Box<dyn std::error::Error>> {
        debug!("Writing as ELF with base address 0x{:x}", addr);
        let mut buf = Vec::new();

        // object is not well documented, see how this API must be used here:
        // https://github.com/gimli-rs/object/blob/master/src/write/elf/object.rs#L217

        let mut writer = object::write::elf::Writer::new(Endianness::Little, false, &mut buf);

        // Calculating offsets

        writer.reserve_file_header();

        let _text_id = writer.reserve_section_index();
        let data_ofs = writer.reserve(data.len(), 4);
        let text_name = writer.add_section_name(".text".as_bytes());

        writer.reserve_null_symbol_index();
        let symtab_num_local = writer.symbol_count();
        writer.reserve_symtab_section_index();
        writer.reserve_symtab();
        writer.reserve_symtab_shndx();
        writer.reserve_strtab_section_index();
        writer.reserve_strtab();
        writer.reserve_shstrtab_section_index();
        writer.reserve_shstrtab();
        writer.reserve_section_headers();

        // Writing

        writer.write_file_header(&FileHeader {
            abi_version: elf::EV_CURRENT,
            os_abi: elf::ELFOSABI_NONE,
            e_type: elf::ET_EXEC,
            e_machine: elf::EM_ARM,
            e_entry: addr.into(),
            e_flags: M33_FLAGS,
        })?;
        writer.write_align(4);
        writer.write(&data);
        writer.write_null_symbol();
        writer.write_symtab_shndx();
        writer.write_strtab();

        writer.write_shstrtab();
        writer.write_null_section_header();

        writer.write_section_header(&SectionHeader {
            name: Some(text_name),
            sh_type: elf::SHT_PROGBITS,
            sh_flags: (elf::SHF_ALLOC | elf::SHF_EXECINSTR).into(),
            sh_addr: addr.into(),
            sh_offset: data_ofs as u64,
            sh_size: data.len() as u64,
            sh_link: elf::SHN_UNDEF.into(),
            sh_info: 0,
            sh_addralign: 4,
            sh_entsize: 0,
        });

        writer.write_symtab_section_header(symtab_num_local);
        writer.write_symtab_shndx_section_header();
        writer.write_strtab_section_header();
        writer.write_shstrtab_section_header();

        output.write_all(&buf)?;

        Ok(())
        // let triple = target_lexicon::Triple {
        //     architecture: Architecture::Arm(target_lexicon::ArmArchitecture::Armv8mBase),
        //     vendor: Vendor::Unknown,
        //     operating_system: OperatingSystem::None_,
        //     environment: Environment::Eabihf,
        //     binary_format: BinaryFormat::Elf,
        // };
    }
}

impl MemoryWriter for IhexWriter {
    fn write_mem(
        &mut self,
        output: &mut dyn std::io::Write,
        addr: u32,
        data: &[u8],
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut records = Vec::new();

        const BLOCK_SIZE: u32 = u16::MAX as u32 + 1;
        const CHUNK_SIZE: u32 = 16;

        for (block, data) in data.chunks(BLOCK_SIZE as usize).enumerate() {
            records.push(ihex::Record::ExtendedLinearAddress(
                (((addr + block as u32 * BLOCK_SIZE) & 0xffff0000) >> 16)
                    .try_into()
                    .unwrap(),
            ));
            records.extend(
                data.chunks(CHUNK_SIZE as usize)
                    .enumerate()
                    .map(|(offset, chunk)| ihex::Record::Data {
                        offset: ((offset as u32 * CHUNK_SIZE) & 0xffff).try_into().unwrap(),
                        value: chunk.to_vec(),
                    }),
            );
        }

        records.push(ihex::Record::EndOfFile {});

        let obj = ihex::create_object_file_representation(&records)?;

        Ok(output.write_all(obj.as_bytes())?)
    }
}

impl MemoryWriter for BinWriter {
    fn write_mem(
        &mut self,
        output: &mut dyn std::io::Write,
        _addr: u32, // raw bin writer doesn't care about address
        data: &[u8],
    ) -> Result<(), Box<dyn std::error::Error>> {
        Ok(output.write_all(data)?)
    }
}

impl ObjectFileReader for BinReader {
    fn new(file: &PathBuf) -> Result<Box<dyn ObjectFileReader>, std::io::Error>
    where
        Self: Sized,
    {
        let file = File::open(file)?;
        let filesize = file.metadata()?.len().try_into().unwrap();
        Ok(Box::new(BinReader { file, filesize }))
    }

    fn read_all(&mut self, size: Option<u32>) -> Result<MemorySection, Box<dyn std::error::Error>> {
        // The casting here is a bit janky...
        let size = std::cmp::min(size.unwrap_or(self.filesize as u32) as usize, self.filesize);
        let mut buf = Vec::from_iter(std::iter::repeat(0).take(size));
        self.file.read_exact(&mut buf)?;

        Ok(MemorySection {
            data: buf,
            address: 0, // Bin file has no address information
        })
    }
}

impl ObjectFileReader for IhexReader {
    fn new(file: &PathBuf) -> Result<Box<dyn ObjectFileReader>, std::io::Error>
    where
        Self: Sized,
    {
        // Parse the entire file up front, since we need it for len() anyway. Only store the parsed sections.
        let file_contents = std::fs::read_to_string(file)?;

        Ok(Box::new(IhexReader { file_contents }))
    }
    fn read_all(&mut self, size: Option<u32>) -> Result<MemorySection, Box<dyn std::error::Error>> {
        let reader = ihex::Reader::new(&self.file_contents);
        let mut file_base = None;
        let mut cur_base = 0;
        let mut data = Vec::new();

        for rec in reader {
            if let Ok(record) = rec {
                match record {
                    ihex::Record::ExtendedLinearAddress(a) => {
                        cur_base = (a as u32) << 16;
                        debug!(
                            "ihex: Got ExtendedLinearAddress, new base: {:08x}",
                            (a as u32) << 16
                        );
                    }
                    // Entry point address is not useful for a flash tool, but it is emitted by objcopy etc. so we need to support/ignore it
                    ihex::Record::StartSegmentAddress { .. }
                    | ihex::Record::StartLinearAddress(_) => (),

                    ihex::Record::Data { offset, mut value } => {
                        let record_base = cur_base + (offset as u32);
                        // File base comes from the address of the first record
                        if file_base.is_none() {
                            file_base = Some(record_base);
                        }
                        if (data.len() as u32) < record_base {
                            // We're going to coalesce disjoint 'sections' in Ihex files, because they don't support 'real' sections to identify
                            // what should or should not be programmed
                            debug!(
                                "Extending vec by {} to take up slack space",
                                (record_base as usize) - data.len()
                            );
                            data.extend(repeat(0xff).take((record_base as usize) - data.len()));
                        }
                        data.append(&mut value);
                        if size.is_some() && data.len() >= size.unwrap() as usize {
                            data.truncate(size.unwrap() as usize);
                            return Ok(MemorySection {
                                data,
                                address: file_base.unwrap_or(0),
                            });
                        }
                    }
                    ihex::Record::EndOfFile => continue,
                    _ => unimplemented!(
                        "Only I32HEX format files are supported. We don't support record type {:?}",
                        record
                    ),
                }
            } else {
                return Err(std::io::Error::new(
                    std::io::ErrorKind::InvalidData,
                    rec.unwrap_err().to_string(),
                )
                .into());
            }
        }

        Ok(MemorySection {
            data,
            address: file_base.unwrap_or(0),
        })
    }
}

//TODO: This is getting a bit hairy to figure out which sections to include, so forget about it for now.

// impl ObjectFileReader for ElfReader {
//     fn new(file: &PathBuf) -> Result<Box<dyn ObjectFileReader>, std::io::Error>
//     where
//         Self: Sized,
//     {
//         let file_contents = std::fs::read(file)?;
//         Ok(Box::new(ElfReader { file_contents }))
//     }
//     fn read_all(&mut self, size: Option<u32>) -> Result<MemorySection, Box<dyn std::error::Error>> {
//         let object = object::File::parse(&*self.file_contents).unwrap();
//         let data = Vec::new();
//         let file_base = None;

//         debug!(
//             "Opened object file, found sections: {:?}",
//             object
//                 .sections()
//                 .map(|x| x.name().unwrap().to_owned())
//                 .collect::<Vec<String>>()
//         );

//         for sec in object.sections() {
//             if let SectionFlags::Elf{ sh_flags: flags } = sec.flags() {
//                 if flags & elf::SHT_PROGBITS {
//                     if file_base.is_none() {
//                         file_base = Some(sec.address());
//                     } else if sec.address() < file_base.unwrap() {
//                         // Make space at the beginning of data
//                         data.
//                         file_base = Some(sec.address());

//                     }

//                 }
//             }
//         }

//         Ok(MemorySection {
//             data: section
//                 .data_range(section.address(), size)?
//                 .ok_or("section is empty or smaller than given size")?
//                 .to_vec(),
//             address: section.address() as u32,
//         })
//     }
// }
