use core::fmt;
use std::{
    ffi::CString, fmt::Write, fs::File, io::BufWriter, os::unix::prelude::OsStrExt, path::PathBuf,
};

use anyhow::{anyhow, Context, Error};
use clap_num::maybe_hex;
use env_logger::Env;
use file::ObjectFileReader;
use log::{debug, error, info, log_enabled, warn, Level};

use hidapi::{DeviceInfo, HidApi};

use crate::file::{MemoryFileType, MemoryWriter};
use clap::{
    builder::{TypedValueParser, ValueParserFactory},
    error::ErrorKind,
    ArgAction, Args, Parser, Subcommand, ValueEnum,
};
use console::style;
use indicatif::{HumanBytes, ProgressBar, ProgressStyle};

pub mod file;
pub mod packet;
pub mod protocol;

#[derive(Debug)]
struct NoDevicesError;
impl std::error::Error for NoDevicesError {
    fn cause(&self) -> Option<&dyn std::error::Error> {
        None
    }
}
impl fmt::Display for NoDevicesError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_str("No devices found")
    }
}

#[derive(Clone, Debug)]
struct VidPid {
    vid: u16,
    pid: u16,
}
impl fmt::Display for VidPid {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.write_fmt(format_args!("{:04x}:{:04x}", self.vid, self.pid))
    }
}

impl ValueParserFactory for VidPid {
    type Parser = VidPidParser;
    fn value_parser() -> Self::Parser {
        VidPidParser
    }
}

#[derive(Clone)]
struct VidPidParser;

impl TypedValueParser for VidPidParser {
    type Value = VidPid;
    fn parse_ref(
        &self,
        cmd: &clap::builder::Command,
        _: Option<&clap::builder::Arg>,
        value: &std::ffi::OsStr,
    ) -> Result<Self::Value, clap::error::Error> {
        let value = value
            .to_str()
            .ok_or(clap::Error::new(ErrorKind::InvalidUtf8).with_cmd(cmd))?;
        if let Some((raw_vid, raw_pid)) = value.split_once(':') {
            if let (Ok(vid), Ok(pid)) = (
                u16::from_str_radix(raw_vid, 16),
                u16::from_str_radix(raw_pid, 16),
            ) {
                return Ok(VidPid { vid, pid });
            }
        }
        Err(clap::Error::new(ErrorKind::InvalidValue).with_cmd(cmd))
    }
}

#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Args, Debug)]
struct UsbDeviceSpecifier {
    /// USB VID:PID as colon-separated pair
    #[arg(short = 'i', long, default_value_t = VidPid{vid:0x1fc9,pid:0x0021}, group = "device")]
    usb_id: VidPid,

    /// hidraw device node path
    #[arg(short, long, group = "device", conflicts_with = "usb_id")]
    dev_path: Option<PathBuf>,
}

#[derive(ValueEnum, Debug, Clone)]
enum ObjectFileType {
    Elf,
    Ihex,
    Bin,
    /// Derive file type from file name (not magic)
    Auto,
}

#[derive(ValueEnum, Debug, Clone)]
enum WriteEraseMethod {
    /// Erase the entire flash
    All,
    /// Erase only the area to be written
    Region,
    /// Don't erase (assume memory is erased)
    None,
}

impl ObjectFileType {
    fn mem_writer(&self, filename: &PathBuf) -> Box<dyn MemoryWriter> {
        match self {
            Self::Auto => MemoryFileType::mem_writer_for_file(filename),
            Self::Bin => MemoryFileType::Bin.mem_writer(),
            Self::Ihex => MemoryFileType::Ihex.mem_writer(),
            Self::Elf => MemoryFileType::Elf.mem_writer(),
        }
    }
    fn file_reader(&self, filename: &PathBuf) -> Result<Box<dyn ObjectFileReader>, std::io::Error> {
        match self {
            Self::Auto => MemoryFileType::object_file_reader_auto(filename),
            Self::Bin => MemoryFileType::Bin.object_file_reader(filename),
            Self::Ihex => MemoryFileType::Ihex.object_file_reader(filename),
            Self::Elf => MemoryFileType::Elf.object_file_reader(filename),
        }
    }
}

#[derive(Args, Debug)]
struct ReadArgs {
    /// Object file to write
    file: PathBuf,
    #[arg(short = 't', long = "type", value_enum, default_value_t = ObjectFileType::Auto)]
    /// Type of object file to generate
    filetype: ObjectFileType,

    /// USB device to act on
    #[command(flatten)]
    devspec: UsbDeviceSpecifier,

    /// Base memory address on microcontroller (defaults to start of flash)
    #[arg(short = 'a', long = "base-address", value_parser=maybe_hex::<u32>)]
    addr: Option<u32>,

    /// Size to read in bytes (defaults to size of flash)
    #[arg(short, long, value_parser=maybe_hex::<u32>)]
    size: Option<u32>,
}

#[derive(Args, Debug)]
struct WriteArgs {
    /// Object file to read
    file: PathBuf,
    #[arg(short = 't', long = "type", value_enum, default_value_t = ObjectFileType::Auto)]
    filetype: ObjectFileType,

    /// USB device to act on
    #[command(flatten)]
    devspec: UsbDeviceSpecifier,

    /// Base memory address on microcontroller to write to (defaults to start of flash)
    #[arg(short = 'a', long = "write-address", value_parser=maybe_hex::<u32>)]
    addr: Option<u32>,

    /// Size to write in bytes (defaults to size of flash)
    #[arg(short, long, value_parser=maybe_hex::<u32>)]
    size: Option<u32>,

    /// Don't reset the microcontroller after writing
    #[arg(short, long)]
    no_reset: bool,

    /// Erase method to use before writing
    #[arg(short, long, default_value = "region")]
    erase: WriteEraseMethod,
}

#[derive(Args, Debug)]
struct ListArgs {
    /// USB device to act on
    #[command(flatten)]
    devspec: UsbDeviceSpecifier,
}

#[derive(Args, Debug)]
struct EraseArgs {
    /// USB device to act on
    #[command(flatten)]
    devspec: UsbDeviceSpecifier,

    #[arg(long = "all", default_value_t = true)]
    /// Erase the entire chip
    erase_all: bool,

    /// Address to start erasing at
    #[arg(short = 'a', long="base-address", value_parser=maybe_hex::<u32>, conflicts_with="erase_all")]
    addr: Option<u32>,

    /// Number of bytes to erase
    #[arg(short, long, value_parser=maybe_hex::<u32>, conflicts_with="erase_all")]
    size: Option<u32>,
}

#[derive(Args, Debug)]
struct ResetArgs {
    #[command(flatten)]
    devspec: UsbDeviceSpecifier,
}

#[derive(Subcommand, Debug)]
enum Commands {
    /// Read microcontroller memory to a file
    Read(ReadArgs),
    /// Write a file to microcontroller memory
    ///
    /// Read the object file, ignoring address mapping and relocation, and write its
    /// read-only sections to the given address (or flash)
    Write(WriteArgs),
    /// Erase the microcontroller's flash
    Erase(EraseArgs),
    /// List available ISP devices
    List(ListArgs),
    /// Reset the microcontroller
    Reset(ResetArgs),
}

fn read_write_style() -> ProgressStyle {
    ProgressStyle::with_template(
        "[{elapsed}] {msg:4}{spinner} {prefix:20} {bar:40.cyan/blue} {bytes:>7}/{total_bytes:7}",
    )
    .unwrap()
    .progress_chars("##-")
}

fn do_erase(
    isp: &protocol::UsbIsp,
    all: bool,
    start_addr: Option<u32>,
    size: Option<u32>,
) -> Result<(), Error> {
    let erase_pb = ProgressBar::new(size.unwrap_or(0).into())
        .with_style(read_write_style())
        .with_prefix("Erasing flash");
    if all {
        erase_pb.set_length(isp.GetFlashSizeInBytes()?.into());
        isp.flash_erase_all(None)?;
        erase_pb.finish();
    } else {
        let flash_start = isp.GetFlashStartAddress()?;
        let flash_size = isp.GetFlashSizeInBytes()?;
        let start_addr = start_addr.unwrap_or(flash_start);
        let size = size.unwrap_or(flash_size);
        if start_addr + size > flash_start + flash_size {
            warn!("Looks like you're attempting to erase beyond the end of flash (0x{:x}-0x{:x})! This will probably fail.", start_addr, start_addr+size);
        }
        isp.flash_erase_region(None, start_addr, size)?;
        erase_pb.finish();
    }
    Ok(())
}

fn write_file_to_flash(args: &WriteArgs) -> Result<(), Error> {
    let isp = connect_device(&args.devspec)?;

    let mut infile = args
        .filetype
        .file_reader(&args.file)
        .with_context(|| format!("Opening {} for reading", args.file.display()))?;

    let contents = infile
        .read_all(None)
        .or_else(|e| Err(anyhow!("Unable to read file contents: {}", e)))?;

    debug!(
        "Input file read (origin 0x{:08x} size 0x{:08x}):",
        contents.address,
        contents.data.len()
    );

    let flash_start = args.addr.unwrap_or(isp.GetFlashStartAddress()?);
    let flash_size = args.size.unwrap_or(contents.data.len().try_into()?);

    debug!(
        "Will start write/erase at 0x{:08x} for 0x{:08x} bytes",
        flash_start, flash_size
    );

    match args.erase {
        WriteEraseMethod::All => {
            do_erase(&isp, true, None, None).with_context(|| "Erasing all flash")?
        }
        WriteEraseMethod::Region => do_erase(&isp, false, Some(flash_start), Some(flash_size))
            .with_context(|| {
                format!(
                    "Erasing flash region 0x{:08x} size 0x{:08x}",
                    flash_start, flash_size
                )
            })?,
        WriteEraseMethod::None => (),
    }
    debug!(
        "Beginning flash write at 0x{:08x} for 0x{:08x}",
        flash_start, flash_size
    );
    let mut data_to_write = &contents.data[..flash_size as usize];
    isp.WriteMemory(
        &mut data_to_write,
        flash_start,
        flash_size,
        Some(protocol::MemoryId::Internal),
    )?;

    if !args.no_reset {
        isp.reset()?;
    }

    Ok(())
}

fn read_flash_to_file(args: &ReadArgs) -> Result<(), Error> {
    let isp = connect_device(&args.devspec)?;

    let flash_start = args.addr.unwrap_or(isp.GetFlashStartAddress()?);
    let flash_size = args.size.unwrap_or(isp.GetFlashSizeInBytes()?);

    let mut buf = Vec::with_capacity(flash_size as usize);

    let read_pb = ProgressBar::new(flash_size.into())
        .with_style(read_write_style())
        .with_prefix("Reading flash");
    // .with_finish(indicatif::ProgressFinish::WithMessage("Done".into()));

    // Allow failure, we might still have some data to write
    match isp.ReadMemory(
        &mut read_pb.wrap_write(&mut buf),
        flash_start,
        flash_size,
        None,
    ) {
        Ok(_) => read_pb.finish_with_message(style("DONE").bold().green().to_string()),
        Err(e) => {
            read_pb.abandon_with_message(style("FAIL").bold().red().to_string());
            error!(
                "reading from flash: {}. Continuing with incomplete data.",
                e
            );
        }
    };

    let mut writer = args.filetype.mem_writer(&args.file);

    let write_pb: ProgressBar = ProgressBar::new(buf.len() as u64)
        .with_style(read_write_style())
        .with_prefix("Writing object file")
        .with_finish(indicatif::ProgressFinish::WithMessage(
            style("DONE").bold().green().to_string().into(),
        ));

    let output = BufWriter::new(File::create(args.file.as_path())?);
    writer
        .write_mem(&mut write_pb.wrap_write(output), flash_start, &buf)
        .or_else(|e| Err(anyhow!("Error writing to object file: {}", e)))?;
    write_pb.finish_using_style();

    Ok(())
}

fn erase_flash(args: &EraseArgs) -> Result<(), Error> {
    let isp = connect_device(&args.devspec)?;
    do_erase(&isp, args.erase_all, args.addr, args.size)
}

fn reset(args: &ResetArgs) -> Result<(), Error> {
    let isp = connect_device(&args.devspec)?;

    isp.reset()
}

fn get_matching_devices<'a>(
    api: &'a hidapi::HidApi,
    devspec: &UsbDeviceSpecifier,
) -> Result<Vec<&'a DeviceInfo>, Error> {
    match &devspec.dev_path {
        Some(path) => {
            let path_ref: &std::ffi::OsStr = path.as_ref();
            let path_str = CString::new(path_ref.as_bytes())?;
            let v: Vec<_> = api
                .device_list()
                .filter(|dev| dev.path().eq(&path_str))
                .collect();
            Ok(v)
        }
        None => {
            let v: Vec<_> = api
                .device_list()
                .filter(|dev| {
                    dev.vendor_id() == devspec.usb_id.vid && dev.product_id() == devspec.usb_id.pid
                })
                .collect();
            Ok(v)
        }
    }
}

fn connect_device(devspec: &UsbDeviceSpecifier) -> Result<protocol::UsbIsp, Error> {
    let api = HidApi::new()?;
    let matches = get_matching_devices(&api, devspec)?;
    if matches.len() < 1 {
        return Err(NoDevicesError {}.into());
    } else if matches.len() > 1 {
        warn!("More than one device matched specifier, connecting to first match.");
    }
    debug!(
        "Connecting to device {} {} at `{}`",
        matches[0].manufacturer_string().unwrap_or(""),
        matches[0].product_string().unwrap_or("device"),
        matches[0].path().to_str()?
    );
    let dev = matches[0].open_device(&api)?;
    let proto = protocol::UsbIsp::new(dev);
    debug!("Connected to device {}", proto.GetUniqueDeviceId()?);
    Ok(proto)
}

fn print_device(api: &HidApi, dev: &DeviceInfo) -> Result<(), Error> {
    let hid_dev = dev.open_device(&api)?;
    let isp = protocol::UsbIsp::new(hid_dev);
    println!(
        "{:<14} [{:04x}:{:04x}] {:<13} {:10} {}",
        style(dev.path().to_str()?).magenta(),
        style(dev.vendor_id()).cyan(),
        style(dev.product_id()).cyan(),
        style(isp.GetSystemDeviceId()?).yellow(),
        style(HumanBytes(isp.GetFlashSizeInBytes()? as u64)).green(),
        style(isp.GetUniqueDeviceId()?).white()
    );
    Ok(())
}

fn print_all_devices(devspec: &UsbDeviceSpecifier) -> Result<(), Error> {
    let api = hidapi::HidApi::new()?;

    let devices: Vec<_> = get_matching_devices(&api, &devspec)?;
    if devices.len() > 0 {
        println!(
            "{:^14}|{:^11}|{:^13}|{:^10}|{:^36}",
            "Path", "VID:PID", "Chip", "Flash", "UUID"
        );
        println!(
            "{:-<14}/{:-<11}/{:-<13}/{:-<10}/{:-<36}",
            "", "", "", "", ""
        );

        for dev in devices {
            print_device(&api, dev)?;
        }
    } else {
        println!("No devices found :(");
    }

    Ok(())
}

fn main() -> Result<(), Error> {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();
    let cli = Cli::parse();

    match cli.command {
        Commands::Read(args) => read_flash_to_file(&args),
        Commands::Write(args) => write_file_to_flash(&args),
        Commands::Erase(args) => erase_flash(&args),
        Commands::Reset(args) => reset(&args),
        Commands::List(args) => print_all_devices(&args.devspec),
    }
}
