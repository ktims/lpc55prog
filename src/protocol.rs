// Allow non snake case to match with NXP documentation
#![allow(non_snake_case)]

use anyhow::{anyhow, Error};
use hidapi::HidDevice;
use log::debug;
use num_enum::{FromPrimitive, IntoPrimitive};
use std::fmt::{Debug, Display};
use std::io::{Read, Write};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use uuid::Uuid;

use crate::packet::{
    self, GetPropertyResponseParams, Packet, ResponsePacket, ResponseParameters, StatusCode,
    UsbPacket,
};
use crate::packet::{CommandFlags, CommandPacket, CommandTag};

const DATA_PACKET_MAX: usize = 56; //TODO: Need to read this from the chip
const READ_TIMEOUT: i32 = 1000;

#[repr(u32)]
#[derive(IntoPrimitive, EnumIter, Debug, PartialEq, Clone)]
pub enum MemoryId {
    Internal = 0x000,
    XoInternal = 0x010,
    QspiNor = 0x001,
    SemcNor = 0x008,
    FlexSpiNor = 0x009,
    SemcRawNand = 0x100,
    FlexSpiNand = 0x101,
    SpiNorEeprom = 0x110,
    Sd = 0x120,
    Emmc = 0x121,
}

#[repr(u8)]
#[derive(IntoPrimitive, EnumIter, Debug, PartialEq, Clone)]
pub enum Property {
    CurrentVersion = 1,
    AvailablePeripherals = 2,
    FlashStartAddress = 3,
    FlashSizeInBytes = 4,
    Availablecommands = 7,
    CheckStatus = 8,
    MaxPacketSize = 0x0b,
    ReservedRegions = 0x0c,
    SystemDeviceId = 0x10,
    LifeCycleState = 0x11,
    UniqueDeviceId = 0x12,
    ExternalMemoryAttributes = 0x19,
    IrqNotifierPin = 0x1c,
}

#[repr(u32)]
#[derive(IntoPrimitive, FromPrimitive, EnumIter, Debug, PartialEq, Clone)]
pub enum LifeCycleState {
    Development = 0x5aa55aa5,
    Deployment = 0xc33cc33c,
    #[num_enum(catch_all)]
    Unknown(u32),
}
#[derive(Debug)]
pub struct BootLoaderVersion {
    pub name: char,
    pub major: u8,
    pub minor: u8,
    pub bugfix: u8,
}

impl From<u32> for BootLoaderVersion {
    fn from(value: u32) -> Self {
        Self {
            name: char::from_u32(value >> 24).unwrap(),
            major: (value >> 16 & 0xff) as u8,
            minor: (value >> 8 & 0xff) as u8,
            bugfix: (value & 0xff) as u8,
        }
    }
}

#[derive(Debug)]
pub struct AvailablePeripherals {
    pub usb_hid: bool,
    pub spi_slave: bool,
    pub i2c_slave: bool,
    pub lpuart: bool,
}

impl From<u32> for AvailablePeripherals {
    fn from(value: u32) -> Self {
        let bits = value;
        Self {
            usb_hid: bits & (1 << 4) != 0,
            spi_slave: bits & (1 << 2) != 0,
            i2c_slave: bits & (1 << 1) != 0,
            lpuart: bits & 1 != 0,
        }
    }
}

pub struct AvailableCommands {
    bits: u32,
}

impl AvailableCommands {
    fn command_available(&self, tag: &packet::CommandTag) -> bool {
        let mask = 1 << (*tag as u8 - 1);
        self.bits & mask != 0
    }
}

impl From<u32> for AvailableCommands {
    fn from(value: u32) -> Self {
        Self { bits: value }
    }
}

impl Debug for AvailableCommands {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_list()
            .entries(packet::CommandTag::iter().filter(|tag| self.command_available(tag)))
            .finish()
    }
}

#[derive(Debug)]
pub struct ReservedRegion {
    start: u32,
    end: u32,
}

#[derive(Debug)]
pub struct ReservedRegions {
    count: u8,
    regions: Vec<ReservedRegion>,
}

impl From<GetPropertyResponseParams> for ReservedRegions {
    fn from(value: GetPropertyResponseParams) -> Self {
        let mut regions = Vec::new();
        let count = value.properties[0] as usize;
        for i in 1..count {
            regions.push(ReservedRegion {
                start: value.properties[i],
                end: value.properties[i + 1],
            });
        }
        Self {
            count: count.try_into().unwrap(),
            regions,
        }
    }
}

#[repr(u32)]
#[derive(IntoPrimitive, FromPrimitive, EnumIter, PartialEq, Clone)]
pub enum LpcDeviceId {
    LPC55S28 = 0xA010119C,
    LPC55S26 = 0xA010229A,
    LPC5528 = 0xA010111C,
    LPC5526 = 0xA010221A,
    #[num_enum(catch_all)]
    Unknown(u32),
}

impl Display for LpcDeviceId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::LPC5526 => write!(f, "LPC5526"),
            Self::LPC5528 => write!(f, "LPC5528"),
            Self::LPC55S26 => write!(f, "LPC55S26"),
            Self::LPC55S28 => write!(f, "LPC55S28"),
            Self::Unknown(i) => write!(f, "0x{:x}", i),
        }
    }
}

#[repr(u32)]
#[derive(IntoPrimitive, EnumIter, PartialEq, Clone)]
pub enum LpcDieId {
    Rev0a = 0x0,
    Rev1b = 0x1,
    #[num_enum(catch_all)]
    Unknown(u32),
}

impl From<u32> for LpcDieId {
    fn from(value: u32) -> Self {
        match value & 0xf {
            0x0 => Self::Rev0a,
            0x1 => Self::Rev1b,
            i => Self::Unknown(i),
        }
    }
}

impl Display for LpcDieId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Rev0a => write!(f, "0A"),
            Self::Rev1b => write!(f, "1B"),
            Self::Unknown(i) => write!(f, "0x{:x}", i),
        }
    }
}

pub struct SystemDeviceId {
    pub device_id: LpcDeviceId,
    pub die_id: LpcDieId,
}
impl From<GetPropertyResponseParams> for SystemDeviceId {
    fn from(value: GetPropertyResponseParams) -> Self {
        Self {
            device_id: value.properties[0].into(),
            die_id: value.properties[1].into(),
        }
    }
}
impl Display for SystemDeviceId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.pad(&format!("{} ({})", self.device_id, self.die_id))
    }
}

#[derive(Debug)]
pub struct IrqNotifierPinSetting {
    pub gpio_pin: u8,
    pub gpio_port: u8,
    pub enabled: bool,
}
impl From<u32> for IrqNotifierPinSetting {
    fn from(bits: u32) -> Self {
        Self {
            gpio_pin: (bits & 0xff) as u8,
            gpio_port: ((bits >> 8) & 0xff) as u8,
            enabled: bits & 0x80000000 != 0,
        }
    }
}
impl Into<u32> for &IrqNotifierPinSetting {
    fn into(self) -> u32 {
        (if self.enabled { 1u32 << 31 } else { 0u32 })
            | ((self.gpio_port as u32) << 8)
            | self.gpio_pin as u32
    }
}

#[derive(Debug)]
pub struct ExternalMemoryAttributes {
    pub start_address: Option<u32>,
    pub size_kb: Option<u32>,
    pub page_size_bytes: Option<u32>,
    pub sector_size_bytes: Option<u32>,
    pub block_size_bytes: Option<u32>,
}

impl From<GetPropertyResponseParams> for ExternalMemoryAttributes {
    fn from(value: GetPropertyResponseParams) -> Self {
        //TODO: use nom here??
        let props = value.properties;
        let supported = u32::from_le(props[0]);
        Self {
            start_address: if supported & 1 != 0 {
                Some(u32::from_le(props[1]))
            } else {
                None
            },
            size_kb: if supported & 2 != 0 {
                Some(u32::from_le(props[2]))
            } else {
                None
            },
            page_size_bytes: if supported & 4 != 0 {
                Some(u32::from_le(props[3]))
            } else {
                None
            },
            sector_size_bytes: if supported & 8 != 0 {
                Some(u32::from_le(props[4]))
            } else {
                None
            },
            block_size_bytes: if supported & 0x10 != 0 {
                Some(u32::from_le(props[5]))
            } else {
                None
            },
        }
    }
}

#[derive(Debug)]
pub struct CheckStatus {
    pub crc: u32,
    pub last_error: u32,
}

impl From<GetPropertyResponseParams> for CheckStatus {
    fn from(value: GetPropertyResponseParams) -> Self {
        Self {
            crc: value.properties[0],
            last_error: value.properties[0],
        }
    }
}

pub struct UsbIsp {
    device: HidDevice,
}

impl UsbIsp {
    pub fn new(device: HidDevice) -> Self {
        device.set_blocking_mode(false).unwrap();
        Self { device }
    }
    pub fn send_command(&self, p: CommandPacket) -> Result<ResponsePacket, Error> {
        debug!("Writing packet: {:?}", p);
        self.write_packet(Packet::CommandPacket(p))
    }
    pub fn write_packet(&self, p: Packet) -> Result<ResponsePacket, Error> {
        let mut expecting_response = true;
        debug!("Writing packet {:?}", &p);
        let usb_packet = match p {
            Packet::CommandPacket(p) => {
                UsbPacket::new(packet::ReportId::CommandOut, Packet::CommandPacket(p))
            }
            Packet::ResponsePacket(_) => panic!("We should not be writing responses"),
            Packet::DataPacket(p) => {
                expecting_response = false;
                UsbPacket::new(packet::ReportId::DataOut, Packet::DataPacket(p))
            }
        };

        let mut buf = Vec::with_capacity(usb_packet.length());
        usb_packet.write(&mut buf)?; // Write packet to buffer
        debug!("raw packet: {:x?}", buf);
        let _ = self.device.write(&buf)?;

        let resp_timeout = if expecting_response { READ_TIMEOUT } else { 0 };

        let mut rbuf = [0u8; 64];
        let res_size = self.device.read_timeout(&mut rbuf[..], resp_timeout)?;
        if res_size > 0 {
            let parsed = packet::usb_packet(&rbuf[..res_size]).unwrap(); //TODO: handle more gracefully, but requires ownership of I
            debug!("Got response {:?}", parsed);

            match parsed.1 {
                Packet::ResponsePacket(p) => Ok(p),
                Packet::CommandPacket(_) => Err(anyhow!("Unexpected command packet on IN stream")),
                Packet::DataPacket(_) => {
                    Err(anyhow!("Data IN packets not expected in reply to commands"))
                }
            }
        } else if !expecting_response {
            Ok(ResponsePacket {
                flags: CommandFlags::empty(),
                tag: packet::ResponseTag::GenericResponse,
                reserved: 0,
                param_count: 2,
                params: ResponseParameters::GenericResponse(packet::GenericResponseParams {
                    status: StatusCode::Success,
                    command: CommandTag::WriteMemory,
                }),
            })
        } else {
            Err(anyhow!("Expected a response and didn't get one"))
        }
    }

    pub fn read_data(&self, buf: &mut impl Write, byte_count: usize) -> Result<(), Error> {
        let mut rbuf = [0u8; 64];
        let mut read = 0;
        while read < byte_count {
            let res_size = self.device.read_timeout(&mut rbuf[..], READ_TIMEOUT)?;
            let parsed = packet::usb_packet(&rbuf[..res_size]).unwrap(); //TODO: handle more gracefully, but requires ownership of I
            match parsed.1 {
                Packet::DataPacket(p) => {
                    read += buf.write(&p.bytes)?;
                }
                Packet::ResponsePacket(r) => match r.params {
                    ResponseParameters::GenericResponse(rp) if rp.status != StatusCode::Success => {
                        // Success here is unexpected, let it fall through to the default case
                        return Err(anyhow!("Error while reading data: {}", rp.status));
                    }
                    _ => {
                        return Err(anyhow!(
                            "Unexpected response params when reading data {:?}",
                            r.params
                        ))
                    }
                },
                p => return Err(anyhow!("Unexpected packet type when reading data {:?}", p)),
            }
        }
        Ok(())
    }

    pub fn write_data(&self, buf: &mut impl Read, byte_count: usize) -> Result<u32, Error> {
        debug!("Writing {} bytes", byte_count);
        let mut tmp_buf = [0u8; DATA_PACKET_MAX];
        let mut written = 0;
        while written < byte_count {
            let to_write = buf.read(&mut tmp_buf)?;
            debug!("{}..{}", written, written + to_write);
            // Avoid the unnecessary copies somehow?
            let resp = self
                .write_packet(Packet::DataPacket(packet::DataPacket {
                    bytes: tmp_buf[..to_write].to_vec(),
                }))?
                .params;

            match resp {
                ResponseParameters::GenericResponse(_) => (),
                _ => Err(anyhow!("Unexpected response during data phase: {:?}", resp))?,
            };

            written += to_write;
        }
        debug!("Wrote total of {} bytes", written);

        // After we're done we expect a GenericResponse with success.
        // For some reason we need to send an empty data packet to finish the write
        let resp = self.write_packet(Packet::DataPacket(packet::DataPacket { bytes: vec![] }))?;
        let params = &resp.params;

        match params {
            ResponseParameters::GenericResponse(rp) if rp.status == StatusCode::Success => Ok(written as u32),
            _ => Err(anyhow!(
                "Got an error response after data phase: {:?}",
                resp
            )),
        }
    }

    pub fn get_last_error(&self) -> u32 {
        // Same as get_property but less checking
        let command = CommandPacket {
            tag: CommandTag::GetProperty,
            flags: CommandFlags::empty(),
            reserved: 0,
            param_count: 1,
            params: vec![Property::CheckStatus as u32],
        };
        let pr = self.send_command(command).ok();
        match pr {
            Some(pr) => match pr.params {
                ResponseParameters::GetPropertyResponse(resp) => match resp.status {
                    packet::StatusCode::Success => CheckStatus::from(resp).last_error,
                    _ => 0,
                },
                _ => 0,
            },
            None => 0,
        }
    }
    pub fn get_property(
        &self,
        prop: Property,
        extra_params: Option<Vec<u32>>,
    ) -> Result<packet::GetPropertyResponseParams, Error> {
        let mut command = CommandPacket {
            tag: CommandTag::GetProperty,
            flags: CommandFlags::empty(),
            reserved: 0,
            param_count: 1,
            params: vec![prop as u32],
        };
        match extra_params {
            Some(v) => {
                command.params.extend(v.iter());
                command.param_count += v.len() as u8;
            }
            None => {}
        }
        let pr = self.send_command(command)?;
        match pr.params {
            ResponseParameters::GetPropertyResponse(resp) => match resp.status {
                packet::StatusCode::Success => Ok(resp),
                code => Err(anyhow!("Error status returned ({})", code)),
            },
            _ => Err(anyhow!(
                "Unexpected response to GetBootloaderVersion command"
            )),
        }
    }
    pub fn set_property(
        &self,
        prop: Property,
        params: Vec<u32>,
    ) -> Result<packet::GenericResponseParams, Error> {
        let command = CommandPacket {
            tag: CommandTag::SetProperty,
            flags: CommandFlags::empty(),
            reserved: 0,
            param_count: 1 + params.len() as u8,
            params: vec![prop as u32],
        };
        let pr = self.send_command(command)?;
        match pr.params {
            ResponseParameters::GenericResponse(resp) => match resp.status {
                packet::StatusCode::Success => Ok(resp),
                code => Err(anyhow!("Error status returned ({:?})", code)),
            },
            _ => Err(anyhow!(
                "Unexpected response to GetBootloaderVersion command"
            )),
        }
    }
    pub fn get_basic_property(
        &self,
        prop: Property,
    ) -> Result<packet::GetPropertyResponseParams, Error> {
        self.get_property(prop, None)
    }

    pub fn flash_erase_all(&self, memory_id: Option<MemoryId>) -> Result<(), Error> {
        let memory_id = memory_id.unwrap_or(MemoryId::Internal);
        let command = CommandPacket {
            tag: CommandTag::FlashEraseAll,
            flags: CommandFlags::empty(),
            reserved: 0,
            param_count: 1,
            params: vec![memory_id.into()],
        };
        self.send_command(command).map(|_| ())
    }

    pub fn flash_erase_region(
        &self,
        memory_id: Option<MemoryId>,
        start_address: u32,
        byte_count: u32,
    ) -> Result<(), Error> {
        if start_address % 4 != 0 || byte_count % 4 != 0 {
            return Err(anyhow!(
                "Erase address and size must be aligned on a 4-byte boundary"
            ));
        }

        let memory_id = memory_id.unwrap_or(MemoryId::Internal);
        let command = CommandPacket {
            tag: CommandTag::FlashEraseRegion,
            flags: CommandFlags::empty(),
            reserved: 0,
            param_count: 3,
            params: vec![start_address, byte_count, memory_id.into()],
        };
        self.send_command(command).map(|_| ())
    }

    pub fn reset(&self) -> Result<(), Error> {
        let command = CommandPacket {
            tag: CommandTag::Reset,
            flags: CommandFlags::empty(),
            reserved: 0,
            param_count: 0,
            params: vec![],
        };
        self.send_command(command).map(|_| ())
    }

    pub fn GetBootloaderVersion(&self) -> Result<BootLoaderVersion, Error> {
        let prop_value = self
            .get_basic_property(Property::CurrentVersion)?
            .properties[0];
        Ok(prop_value.into())
    }
    pub fn GetAvailablePeripherals(&self) -> Result<AvailablePeripherals, Error> {
        let prop_value = self
            .get_basic_property(Property::AvailablePeripherals)?
            .properties[0];
        Ok(prop_value.into())
    }
    pub fn GetAvailableCommands(&self) -> Result<AvailableCommands, Error> {
        let prop_value = self
            .get_basic_property(Property::Availablecommands)?
            .properties[0];
        Ok(prop_value.into())
    }
    pub fn GetFlashStartAddress(&self) -> Result<u32, Error> {
        let prop_value = self.get_basic_property(Property::FlashStartAddress)?;
        Ok(prop_value.properties[0])
    }
    pub fn GetFlashSizeInBytes(&self) -> Result<u32, Error> {
        let prop_value = self.get_basic_property(Property::FlashSizeInBytes)?;
        Ok(prop_value.properties[0])
    }
    pub fn GetMaxPacketSize(&self) -> Result<u32, Error> {
        let prop_value = self.get_basic_property(Property::MaxPacketSize)?;
        Ok(prop_value.properties[0])
    }
    pub fn GetReservedRegions(&self) -> Result<ReservedRegions, Error> {
        let prop_value = self.get_basic_property(Property::ReservedRegions)?;
        Ok(prop_value.into())
    }
    pub fn GetSystemDeviceId(&self) -> Result<SystemDeviceId, Error> {
        let prop_value = self.get_basic_property(Property::SystemDeviceId)?;
        Ok(prop_value.into())
    }
    pub fn GetLifeCycleState(&self) -> Result<LifeCycleState, Error> {
        let prop_value = self.get_basic_property(Property::LifeCycleState)?;
        Ok(LifeCycleState::try_from(prop_value.properties[0])?)
    }
    pub fn GetUniqueDeviceId(&self) -> Result<Uuid, Error> {
        let prop_value = self.get_basic_property(Property::UniqueDeviceId)?;
        let mut buf = [0u8; 16];
        for i in 0..4 {
            buf[i * 4..i * 4 + 4].copy_from_slice(&prop_value.properties[i].to_ne_bytes());
        }
        Ok(Uuid::from_bytes(buf))
    }
    pub fn GetIrqNotifierPin(&self) -> Result<IrqNotifierPinSetting, Error> {
        let prop_value = self.get_basic_property(Property::IrqNotifierPin)?;
        Ok(prop_value.properties[0].into())
    }
    pub fn SetIrqNotifierPin(&self, setting: &IrqNotifierPinSetting) -> Result<(), Error> {
        let command = CommandPacket {
            tag: CommandTag::SetProperty,
            flags: CommandFlags::empty(),
            reserved: 0,
            param_count: 2,
            params: vec![Property::IrqNotifierPin as u32, setting.into()],
        };
        let pr = self.send_command(command.clone())?;
        match pr.params {
            ResponseParameters::GenericResponse(resp) => match resp.status {
                packet::StatusCode::Success => Ok(()),
                code => Err(anyhow!("Error status returned ({:?})", code)),
            },
            _ => Err(anyhow!(
                "Unexpected response to {:?} command ({:?})",
                command.tag,
                pr.params
            )),
        }
    }
    pub fn GetExternalMemoryAttributes(
        &self,
        id: MemoryId,
    ) -> Result<ExternalMemoryAttributes, Error> {
        let prop_value =
            self.get_property(Property::ExternalMemoryAttributes, Some(vec![id as u32]))?;
        Ok(prop_value.into())
    }

    pub fn ReadMemory(
        &self,
        buf: &mut impl std::io::Write,
        start_address: u32,
        byte_count: u32,
        memory_id: Option<MemoryId>,
    ) -> Result<u32, Error> {
        let command = CommandPacket {
            tag: CommandTag::ReadMemory,
            flags: CommandFlags::empty(),
            reserved: 0,
            param_count: 3,
            params: vec![
                start_address,
                byte_count,
                memory_id.unwrap_or(MemoryId::Internal).into(),
            ],
        };
        let pr = self.send_command(command)?;
        let params = match pr.params {
            ResponseParameters::ReadMemoryResponse(params) => match params.status {
                StatusCode::Success => params,
                _ => return Err(anyhow!("Error returned from device: {:?}", params.status)),
            },
            _ => return Err(anyhow!("Unexpected reply to ReadMemory {:?}", pr.params)),
        };
        self.read_data(buf, params.data_bytes as usize)?;

        // We expect a GenericResponse with success after the transfer
        let mut rbuf = vec![0u8; 64];
        let res_size = self.device.read_timeout(&mut rbuf[..], READ_TIMEOUT)?;
        let parsed = packet::usb_packet(&rbuf[..res_size]).unwrap().1; //TODO: handle more gracefully, but requires ownership of I

        match parsed {
            Packet::ResponsePacket(p) => match p.params {
                ResponseParameters::GenericResponse(rp) => match rp.status {
                    StatusCode::Success => Ok(params.data_bytes),
                    _ => Err(anyhow!("After read memory transfer: {}", rp.status)),
                },
                _ => Err(anyhow!("Unexpected response after read memory: {:?}", p)),
            },
            _ => Err(anyhow!(
                "Unexpected packet type after read memory: {:?}",
                parsed
            )),
        }
    }

    pub fn WriteMemory(
        &self,
        buf: &mut impl std::io::Read,
        start_address: u32,
        byte_count: u32,
        memory_id: Option<MemoryId>,
    ) -> Result<u32, Error> {
        let command = CommandPacket {
            tag: CommandTag::WriteMemory,
            flags: CommandFlags::DATA_FOLLOWS,
            reserved: 0,
            param_count: 3,
            params: vec![
                start_address,
                byte_count,
                memory_id.unwrap_or(MemoryId::Internal).into(),
            ],
        };
        let pr = self.write_packet(Packet::CommandPacket(command))?;

        match pr.params {
            ResponseParameters::GenericResponse(params) => match params.status {
                StatusCode::Success => (),
                _ => return Err(anyhow!("Error returned from device: {:?}", params.status)),
            },
            _ => return Err(anyhow!("Unexpected reply to WriteMemory {:?}", pr.params)),
        };
        Ok(self.write_data(buf, byte_count as usize)?)
    }
}
